
from django.urls import path
from .views import CreateCustomerAPIView

urlpatterns = [
    path('create/', CreateCustomerAPIView.as_view(), name = 'create')
]